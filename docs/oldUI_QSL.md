# Pull in an existing question set
You can use question sets that have already been created by other groups by selecting `Search question library` in the list of question types. This allows for better data comparability and saves time in the survey creation process. If you know of another group collecting similar data make sure to check here before you start creating a survey from scratch!

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/add_question.png)  

After selecting `Search question library` some of the question sets will be listed, or you can enter key terms in the search bar.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/search_library.JPG)

Selecting a set will open a description and preview along with the maintainers. If you have questions about the set you should contact the maintainers. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/library_example.JPG)

Select `Add to survey` if you want to use the question set. It will appear as a set of green highlighted questions in your survey. The survey creator has the ability to change some things in the question set such as the labels, but data names will stay the same as in the original question set. Some questions within the set will have a checkbox option to hide the question which makes it invisible to the submitter. The ability to hide or modify questions is dictated by the question set creator. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/add_set.JPG)

When a question set is updated by the owner it does not automatically update in the surveys it is embedded in. Instead the creator of a survey that uses a question set needs to manually go into the survey editor and update the question set(s).

When a new version of a question set is available, a message will appear in the survey editor that lets the editor know there is a question set that is out of date. The editor can then scroll to the question set and select the update button in the upper right hand corner of the question set. Selecting the update button will open a pop-up that lists the previous versions of the survey and the changes that will be made with the current update. Most of the time you will want to keep the `changes only` option selected as this will save most of the modifications you have made to the question set. When you have reviewed everything select update. 

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/question%20set%20updates/update_message.png)
![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/question%20set%20updates/update_details.png)

While most modifications you make will be pushed through to the updated version there are some cases where this is not possible. To ensure that your modifications do not get lost we recommend opening a new window with the same survey opened in the editor so that you can compare the old version of the question set to the new version and make sure all of your modifications are still there.