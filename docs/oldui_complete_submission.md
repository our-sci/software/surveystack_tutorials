# Complete a Submission

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

You can access surveys from your group's homescreen or from the `Browse` tab in the Side Menu, follow a link if one was sent to you, or select a survey from the SurveyStack homepage.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/Complete%20a%20submission/surveystack_homescreen1.png)  

The browse screen allows you to search for surveys in your group or select a pinned survey. You can also use the other tabs (MY SURVEYS, MY GROUPS, or ALL GROUPS) to search for surveys outside of your group. 

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/Complete%20a%20submission/browse2.png)

- Select `Start Survey` to begin your submission. If you are an admin filling out the survey for one of your group members select the option to 'Start survey as member'. This is especially helpful if you are pushing data to farmOS and need access to field and planting names for a producer in the submission.  
![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/Complete%20a%20submission/landing_page3.png)

- Follow the prompts to navigate through the survey and answer questions.
- Some questions are **Private**, they will have a blue eye icon (`This answer is private`). The answers to these questions are only visible to the person submitting the survey or the group administrator.
- Some questions are **Required**, they will have a red asterisk (`Answer required`). You will not be able to select the `NEXT` button and advance through the survey until answer the question.  

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/Complete%20a%20submission/private_required4.png)  
  
- To answer dropdown questions, either select the down arrow in the answer box to open and scroll through the answer list *OR* start typing in the answer box and the app will auto-suggest responses.  

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Complete%20a%20survey/dropdown_questions.png "examples of text, multiple choice, and checkbox questions with arrows pointing to private and required labels.")

- You can pick the location by selecting your location *OR* by moving the map so that the red pin in the center of the map is over the correct location.
     - If the red pin and your location do not match, you can select the `target` icon in the upper right corner of the map. 
- Once you have the red pin over the correct location, select the `PICK` button below the map.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Complete%20a%20survey/location_questions.png)

- To answer matrix questions
     - Select `Add row` and enter/select values for each column.
     - You can also delete and duplicate rows using the icons on the right side of the row.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Complete%20a%20survey/matrix_questions.png)

### Review a survey
- After you have completed the survey, the *survey outline* screen will give you the opportunity to review your answers before submitting the survey. 
     - green checkmarks mean you answered the question
     - an empty circle means that you did not answer the question
     - an orange circle with an exclamation point means that you did not answer a *required* question.
- You can also access the *survey outline* from any question screen by selecting the *outline* icon in the upper right corner of the question screen.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Complete%20a%20survey/survey_overview.png)

### Submit a survey
Once you are satisfied that the survey was completed correctly, you can select `SUBMIT` to submit the survey to the database. Please make sure you are logged into your account before submitting. This will make it easier to track down submissions in the future.  

- **Online Submission**: If you have internet connection, you should get a green message: `Successful Submission`

- **Offline Submission**: If you are **offline** you will get a red error message: `Error: Network Error`. Select `OK` and your submission will be **saved to your device as a draft**. To submit the survey:

    - When you have internet connection, go to `My Submissions` from the side menu.

    - If you see a `SUBMIT COMPLETED` button at the top of the page, there are completed surveys that are ready to submit (these drafts will have a `cloud` icon next to the submission).

    - Select the `SUBMIT COMPLETED`.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Complete%20a%20survey/submit_survey.png)

