# Create an account  

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>


- [Register a new account](#register-a-new-account): Use the `Register now` button to create a new account when arriving at app.surveystack.io
- [Accept an Invitation](#accept-an-invitation): Most users join SurveyStack when they are invited to join a group. To accept an invitation, follow the instructions in the invitation email from invitation@surveystack.io
- [Login](#logging-in): Users can use passwordless login by requesting that a login link is sent to their email or can login using a password
- [Create or change password](#create-or-change-password): A password is not required for SurveyStack, but you can create or change your password by selecting the `profile` icon in the upper right and selecting `Profile`
- [Group Memberships](#manage-group-memberships): Review your group memberships, choose to `Leave` a group



### Register a new account
If you do not have an account and have not been invited to join from an existing SurveyStack group, you can create an account by going to app.surveystack.io and using the `Register now` button. 

Once you have registered your account, you will need to [create a group](https://our-sci.gitlab.io/software/surveystack_tutorials/create_group/) to [create surveys](https://our-sci.gitlab.io/software/surveystack_tutorials/create_survey_quick/).

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/log_in_page.png)

### Accept an invitation
In many cases, users are added to a SurveyStack group by a group/project administator. In this case, you'll have received an invitation from invitation@surveystack.io (example email below). Click on the `Sign In` button to see your group's surveys.   

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/2023_surveystack_group_email_invitation.PNG)

### Logging in
To login to SurveyStack you can use a password or opt to use the *Passwordless* login features. For passwordless login, select the `EMAIL ME A SIGN IN LINK` from the login screen. 

***Notes:*** 
- *If you were invited to SurveyStack, you will not have a password unless you choose to create one (see **Create or change password** below), but creating a password is not required.*
- *You always have the choice to use passwordless login, even if you have created a password.*  

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/log_in_page.png)



### Create or change password  
To create or change a password you must be logged in to your SurveyStack account. 

1. Navigate to the profile by selecting the person icon in the upper right. 
2. Select `profile` from the list. 
3. Update your password in the password control panel.
4. `Save Changes`.  



### Manage Group Memberships
1. Navigate to the profile by selecting the person icon in the upper right. 
2. Select `profile` from the list. 
3. Use `Group Membership` dropdown list to review your group memberships.
4. To leave a group, select it from the dropdown list and select `Leave`


![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/profile_actions.jpg)