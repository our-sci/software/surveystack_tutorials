# Welcome to Surveystack

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

SurveyStack is a flexible Progressive Web App that allows you to create surveys and scripts, manage communities, and collect and analyze data all in a single location.

SurveyStack is cross-platform, meaning that it works on computers and mobile devices, android and iPhone.

Try out [this survey](https://app.surveystack.io/groups/5f2d878c113ed90001f47056/surveys/66ae2cd818cbfcaa6563cfee/submissions/new) to learn more about SurveyStack and its features.

Check out our quick start guides to get started:

- [Create an Account](https://our-sci.gitlab.io/software/surveystack_tutorials/create_surveystack_account/)
- [Navigate Group Workspaces](https://our-sci.gitlab.io/software/surveystack_tutorials/navigate_group_workspace/)
- [Create a Group](https://our-sci.gitlab.io/software/surveystack_tutorials/create_group/)
- [Respond to a Survey](https://our-sci.gitlab.io/software/surveystack_tutorials/respond_to_a_survey/)
- [Download the SurveyStack App](https://our-sci.gitlab.io/software/surveystack_tutorials/download_app/)
- [Create a Survey](https://our-sci.gitlab.io/software/surveystack_tutorials/create_survey_quick/)
