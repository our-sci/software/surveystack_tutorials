# Create a group

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

If you have registered a new account, once you have logged in you will land on the `Groups` page. This page will be empty because you do not have any groups yet. Select `Create a Group` to create your first group.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/groups_empty.jpg)

If you are a member of a group(s), you can navigate to the `Groups` page by selecting `All Groups` from the group selector (upper left). From the groups page, select `Create a Group`.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/create_with_groups.jpg)

Once in the `Create Group` page, enter the name of the new group. *Note: The create group tool will autofill the group slug, but you can also choose to create your own URL friendly version of your group name.*

Select `Create` to finish creating your group. 

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/Create_group.jpg)

Now you can [add members](https://our-sci.gitlab.io/software/surveystack_tutorials/manage_a_group/#adding-new-members) or [create your first survey.](https://our-sci.gitlab.io/software/surveystack_tutorials/create_survey_quick/)

