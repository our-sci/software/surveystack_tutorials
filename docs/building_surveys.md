# Building surveys

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

Select `Create New Survey` from the `All Surveys` page of your group workspace to open up the survey builder. Check out the components of the survey builder in the image and table below.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/survey_components.jpg)


|# |Component|Description|
|:---| :-------------: |:----------------------------|
|1|Expand/Collapse|Expand to hide the sidebar|
|2|Survey name|                               |
|3|[Survey Details](#survey-details)|Define which group and users can access the survey and add a description|
|4|[Resources](#resources)|View which resources (ontology lists and images) are saved to the survey <br> and add new resources|
|5|[Additional <br> survey options](#additional-survey-options)|Import/export the survey has a json, manage survey versions, add survey to <br> the Question set library, set print settings or delete the survey|
|6|Update|Updates the survey, changes will be visible to users filling out surveys, but <br> the change does not create a new version of the survey|
|7|Publish|A survey draft is not visible to non-admin users until it has been published. <br> In other words, you must `publish` a survey before users can create responses.|
|8|Save|It is best to frequently `Save` while creating a new survey. Saving creates a <br> draft that admin users can preview in the Survey Preview tab. This allows <br> survey creators to test the survey in real-time, and to rapidly iterate to <br> improve the user experience.|
|9|[Clone question](#clone-or-delete-questions)|Make a copy of a question|
|10|[Delete Question](#clone-or-delete-questions)|Delete a question|
|11|[Add Question](https://our-sci.gitlab.io/software/surveystack_tutorials/question_types/)|Select the question type to add to the survey|
|12|Label|This text is above the question boxes|
|13|Value|This will be passed on in the json object and will be the column header in <br> the results table|
|14|Hint|This text will be directly above multiple choice answer options or will be in <br> the answer box for text, number and dropdown questions|
|15|More info|This text is below the answer box or the multiple choice options list and <br> should only be used to provide additional directions when the Label and <br> Hint are not sufficient|
|16|Question resources|answer/ontology lists for multiple choice and multi-select questions|
|17|Default Value|Some question types allow for a default value. This value will show up when <br> the user is filling out the survey, but they will be able to change it if needed|
|18|Allow custom <br> answer|Checking the box will allow users to enter custom answers into an `other` <br> box for certain question types|
|19|Required|By checking the required box, a user cannot advance in the survey until <br> they have answered the question|
|20|Private|If this box is checked, answers to these questions will not be visible to the <br> public. Only the person who submitted the survey or an administrator for <br> the group can see private data|
|21|Advanced|Choose Advanced settings like [Relevance](https://our-sci.gitlab.io/software/surveystack_tutorials/relevance_expressions/), [Initialize](https://our-sci.gitlab.io/software/surveystack_tutorials/initialization/), or [API compose](https://our-sci.gitlab.io/software/surveystack_tutorials/api_compose/) to add <br> survey logic, pre-populate answers or make API calls to other services|
|22|Print layout|Select settings for printing pdf version of the survey|
|23|Select View|Preview the survey as it would appear on either a desktop or mobile device|

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/survey_components.jpg)

### Survey Details
1. Assign survey to group - survey submissions will be assigned to this group. *In the future, you will be able to assign surveys to multiple groups.*
2. Select who is able to submit responses  
    - Everyone - Anyone with the link to the survey can submit a response.  
    - Logged in Users - Anyone with a SurveyStack account can submit a response.  
    - Group Members - Only members of your group can submit a response.
3. Add a description of your survey, which will be visible from the survey’s `Description` page.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/Create%20a%20new%20survey/survey_details3.png)  

### Resources
You can add ontology lists at the beginning of your survey for use in dropdown questions, or add them when creating dropdown questions.  
1. Select `+ Create Ontology`.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/Create%20a%20new%20survey/resources5.png)  

2\. Add **List Label**: Name that will be visible in the dropdown list.  
3\. Add **List Data Name**: Name of the resource that will be accessible within the survey.  
4\. Create the list row by row or add rows to an existing list.  
5\. **Actions**: You can move, duplicate or delete items using the action buttons.  
6\. Upload a csv file to create a list.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Create%20new%20survey/upload_csv.png) 

### Additional Survey options
|Option|Description|
|-------------|------------------------------------|
|Import|Import a json survey object to create a copy of a survey|
|Export|Export a json survey object to share a copy of a survey|
|[Manage Survey Versions](#manage-versions)|View survey changes or delete old versions of the survey|
|[Add to Library](https://our-sci.gitlab.io/software/surveystack_tutorials/question_set_library/#creating-a-question-set)|Add the survey to the question set library|
|Print Settings|Define settings for printing completed survey responses as a PDF|
|Delete|Delete the survey|


#### Manage Versions
Each time a survey is republished a new version is created. Having too many verisons of a survey can make the performance slower. In order to combat this you can delete old survey versions/revisions that do not have associated submissions or are referenced by Question Set Libraries. Also, try to group changes together before you publish and try to publish when people are not actively using the survey. For example if you have a yearly survey that people fill out in the Fall/Winter make your edits in the Spring.

To manage survey versions:
1. Click the three dots next to the survey name in the builder and select `Manage survey versions`  
2. Select any versions you want to delete.  
3. Click the arrows next to two versions to pull up a list of changes including questions added, removed, or changed.

![Versioning](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/versioning.png)



### Clone or Delete Questions
Questions can be copied or deleted by selecting the `copy` or `delete` icons on a selected question. When a question is copied, `_copy` is added to the end of the `data_name`, and `Label` is renamed to `copy`. These can be edited to make the `Data name` unique and to update the label.

Groups can also be copied or deleted using the same process. When copying a group, **only the group `Data name` is changed, all questions in the group keep there name.** 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Clone%20or%20Delete/duplicate.png "example of copying a question in the survey builder")



