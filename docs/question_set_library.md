# Question Set Library

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

A key design goal of SurveyStack is to enable easier collaboration both within a group and *between* groups. The **Question Set Library** provides a set of features that help foster that collaboration:

- Share methods or protocols by publishing them in the **Question Set Library**.
- Improve data comparability between surveys by using common sets of questions.

When adding a Question Set from the Library, survey creators have the flexibility to add their own questions and, if allowed in the question set [settings](#question-set-settings), to hide or modify questions.

### Creating a Question Set
If you have expertise in an area that may be useful to others and want to contribute to the question set library the first step is to [create a survey](https://our-sci.gitlab.io/software/surveystack_tutorials/create_survey_quick/) that only contains the questions you want in the set.

When you've finished creating and thoroughly testing your question set survey, select the three dots next to the survey name and click `Add to library`. The survey will then prompt you to add information (description, maintainers, applications) about the question set before adding it to the library. ***Please make sure you have thoroughly tested the survey logic before adding it to the library.*** It is much easier to make changes before adding it to the library. 

![add to library](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/add_to_library.JPG)

#### Question Set Settings
A question set saved to the library is basically a survey, and like any other survey it may have dependencies (ex: `Relevance` or `Initialize`) that can break if it is modified too much. 

After adding the survey to the library you can go through and decide if you want to allow question set users to hide or modify each question. 

- **Hide** questions: Allows survey creators to hide questions when adding the question set to their survey. 
- **Modify** questions: Allows survey creators to modify questions when adding the question set to their survey. For example, survey creators can add or replace answers (multiple choice/select multiple) or replace ontology lists.

***Note**: Survey creators always have the ability to modify the question language (Label, Hint and More Info) regardless of the options selected above.*
![allow modify](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/allow%20modify.png)

### Adding Questions from the Library to a survey
You can use question sets that have already been created by other groups by selecting `Search Question Library` in the list of question types in the survey builder. 

After selecting `Search question library` some of the question sets will be listed, or you can enter key terms in the search bar.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/search_library.JPG)

Selecting a set will open a description and preview along with the maintainers. If you have questions about the set you should contact the maintainers. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/library_example.JPG)

Select `Add to survey` if you want to use the question set. It will appear as a set of green highlighted questions in your survey. The survey creator has the ability to change some things in the question set such as the labels, but data names will stay the same as in the original question set. Some questions within the set will have a checkbox option to hide the question which makes it invisible to the submitter. The ability to hide or modify questions is dictated by the question set creator. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/add_set.JPG)

When a question set is updated by the owner it does not automatically update in the surveys it is embedded in. Instead the creator of a survey that uses a question set needs to manually go into the survey editor and update the question set(s).

When a new version of a question set is available, a message will appear in the survey editor that lets the editor know there is a question set that is out of date. The editor can then scroll to the question set and select the update button in the upper right hand corner of the question set. Selecting the update button will open a pop-up that lists the previous versions of the survey and the changes that will be made with the current update. Most of the time you will want to keep the `changes only` option selected as this will save most of the modifications you have made to the question set. When you have reviewed everything select update. 

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/question%20set%20updates/update_message.png)

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/question%20set%20updates/update_details.png)

While most modifications you make will be pushed through to the updated version there are some cases where this is not possible. To ensure that your modifications do not get lost we recommend opening a new window with the same survey opened in the editor so that you can compare the old version of the question set to the new version and make sure all of your modifications are still there.

### Updating a Question Set
When you modify a question set, include information about the changes and version history so that those users will know what to expect. 

If the maintainers of a question set have published changes, users who have that question set in a survey then have some options for updates. 

Here is a video walking through the process of updating a question set: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/qy6zVnJxL6w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


