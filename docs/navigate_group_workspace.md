# Navigate a Group Workspace

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

SurveyStack is designed to empower **community knowledge**. This design principle means that the app is built around:

- **Groups:** a set of users (people or institutions) who are collaborating.
- **Group Workspaces:** where group members share surveys, add members and member integrations, set permissions about who can create/edit surveys and who can view/edit survey responses and private data.

## Choose Group Workspace
![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/Navigate_to_group_workspace.jpg)

## Workspace actions
Once at the group workspace, the actions that you can take are determined by your role:

### Group Roles
- **Member:** Regular users, can respond to surveys and view and edit their responses and view public results.
- **Admin:** Admins can add/subtract members, create surveys, view and eduit all group responses (survey responses submitted by ANY member of the group), and manage group integrations. For more details on admin actions, go to [Manage a Group](https://our-sci.gitlab.io/software/surveystack_tutorials/manage_a_group/).  



### Actions
1. Select the appropriate option from the sidebar for a more complete list of group resources.
2. Click on the 3-dot menu to take a specific action. The menu options available will be based on your permissions. You can view your permission level in the upper left.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/Workspace_navigation.jpg)
#### Responses
- `My Draft Responses`: View your partially completed survey responses. [Complete](https://our-sci.gitlab.io/software/surveystack_tutorials/manage_my_responses/#complete-a-draft) or delete those drafts. Up to two of the most recent drafts will be visible in the sidebar menu for quick access.
- `My Responses`: View or [edit](https://our-sci.gitlab.io/software/surveystack_tutorials/manage_my_responses/#edit-a-completed-response) your completed survey responses. 
- `Group Responses`: View all survey responses by the group. Admin's can edit these responses.

#### Surveys
- `All Surveys`: View group surveys and take the following [actions:](https://our-sci.gitlab.io/software/surveystack_tutorials/survey_actions/)
     - Start survey
     - Start survey as member (admin only)
     - Call for responses (admin only)
     - View description
     - Print blank survey
     - Edit (admin only)
     - View results
- [Create a new survey](https://our-sci.gitlab.io/software/surveystack_tutorials/create_survey_quick/)
- The top two surveys will be available in the sidebar menu for quick access.

#### Manage group (admin only)
- View [question sets](https://our-sci.gitlab.io/software/surveystack_tutorials/question_set_library/) 
- [View/create/edit scripts](https://our-sci.gitlab.io/software/surveystack_tutorials/question_types/#scripts)
- [Add/subtract members](https://our-sci.gitlab.io/software/surveystack_tutorials/manage_a_group/#adding-new-members)
- [Manage group settings]((https://our-sci.gitlab.io/software/surveystack_tutorials/manage_a_group/) ) (ex: subgroups and integrations) [Link to detailed tutorial] 




