# Batch Updating Survey Images

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>



If you use a SurveyStack survey with lots of images, and those images need to be updated with correct information periodically, here’s the process for making those changes without having to find and replace each image individually in your survey(s). 

SurveyStack includes images by URL link, rather than uploading the image directly into the survey. This keeps our survey sizes and load times smaller, and allows users to decide where their images should be hosted. 

This means that with two simple steps, you can change and update the images in your surveys without ever opening the edit screen. 

1. Give your image files complete descriptive names that describe their function and/or location in your survey

2. When it’s time to update your survey images, replace the files in your hosting location with updated images using the same name. 

For example, rather than naming your image files ‘2022plantingfield” or “fdhtyssdf19”, a name like “vegetable_field_planting” may make more sense– then you can identify each image easily, and update from the past version to the current version without changing the hosting URL. Here’s what that looks like in a practical example: 

![Image Editor](https://gitlab.com/our-sci/resources/-/raw/3cd544b2f45d6fb6bab6ad18199a26a2d2e11746/images/Surveystack%20tutorials/Image%20Replace/imageeditor.png)
*Image URL within the survey builder*

![Image URL](https://gitlab.com/our-sci/resources/-/raw/3cd544b2f45d6fb6bab6ad18199a26a2d2e11746/images/Surveystack%20tutorials/Image%20Replace/surveyimageurl.png)
*Image URL in browser - the image in your survey will be whatever is stored at this link location.*

This lets us navigate to the hosting location and replace all relevant images there, and they’ll be updated anywhere that uses the same URL– even if it’s used across multiple surveys, or multiple times in the same survey. 

![Image in GitLab](https://gitlab.com/our-sci/resources/-/raw/3cd544b2f45d6fb6bab6ad18199a26a2d2e11746/images/Surveystack%20tutorials/Image%20Replace/veg_planting_git.png)

For surveys designed by Our-Sci, we host images in a [common Resources repo on GitLab](https://gitlab.com/our-sci/resources). This gives me an additional way to simplify the updating process – I can store a cloned version of the hosting folder to my personal hard drive, replace all the necessary images in that folder, then push all changes back to the source using a management tool like [GitKraken](https://www.gitkraken.com/): 

![Image in folder](https://gitlab.com/our-sci/resources/-/raw/3cd544b2f45d6fb6bab6ad18199a26a2d2e11746/images/Surveystack%20tutorials/Image%20Replace/shbs_folder.png)

If you’re interested in a resource management process like this one and would like additional support, please reach out to vic@our-sci.net. I’d love to hear from you! 