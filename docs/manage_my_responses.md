# Manage My Responses

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

Group members can manage their responses in two ways:

- Save a draft of a survey response and [complete that draft](#complete-a-draft) later.
- [Edit a survey response after if has been submitted](#edit-a-completed-response). 

### Complete a Draft
There are two ways to find a draft survey:  
1. Ensure you are in the correct group in the top left menu, then click on `My Draft Responses`, find the draft you're looking for and click `CONTINUE` (buttons highlighted in red, below).  
2. If it is your most recent draft, it will likely appear above the `My Draft Responses` button (example survey highlighted in orange, below). Click the three dots and select `CONTINUE`.  

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/complete_a_draft_survey.PNG)

### Edit a completed response  
1. Navigate to the correct group workspace (Responses are only visible in the Group Workspace where they were submitted)  
2. Select `My Responses` from the side menu.  
3. Select the response that you wish to edit.  
4. Choose the reason for re-submiting the response.  
5. Select `EDIT ANYWAY`.  
6. This will open the *Survey outline*, navigate to the questions you'd like to edit, after completing your edits, `SUBMIT` the response.   

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/edit_survey_response.PNG)
![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/resubmit_survey.jpg)