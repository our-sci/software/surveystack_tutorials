# Save and Publish Surveys

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

### Save
It is best to frequently `Save` while creating a new survey using the save button at the top of the `Survey Outline`. Saving creates a draft that admin users can preview in the `Survey Preview` tab. This allows survey creators to test the survey in real-time, and to rapidly iterate to improve the user experience.

### Publish
A survey draft is not visible to non-admin users until it has been published. In other words you must `publish` a survey before users can create submissions. If you make edits to a survey that has already been published any previous submissions will not change based on the edits, they will be the version of the survey that was published at the time of submission. Republishing a survey too many times can make the performance slower (see Managing versions below). Try to group changes together before you publish and try to publish when people are not actively using the survey. For example if you have  a yearly survey that people fill out in the Fall/Winter make your edits in the Spring.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Save%20and%20Publish%20Surveys/save_and_publish.png "screenshot of survey builder showing save and publish buttons")

### Manage Versions
As mentioned above, having too many versions of the survey can slow down performance. In order to combat this you can delete old survey versions/revisions that do not have associated submissions or are referenced by Question Set Libraries. 

To manage survey versions:
1. Click the three dots next to the survey name in the builder and select `Manage survey versions`  
2. Select any versions you want to delete.  
3. Click the arrows next to two versions to pull up a list of changes including questions added, removed, or changed.

![Versioning](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/versioning.png)

You can also add your survey to the question set library by selecting the three dots next to the survey name in the builder and selecting `Add to library`. This is a great tool to share surveys with other groups that are collecting the same information as you because it allows for easy data comparability. 

![library](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Save%20and%20Publish%20Surveys/question_library.png)
