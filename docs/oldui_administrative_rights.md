### Administrative Rights

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

| Actions or Permissions | Admin | User |
|---------------------|-----|----|
| Fill out surveys and submit data |  X  |  X  |
| Create or edit a survey |  X  |   |
| Create or edit a script |  X  |  |
| Invite a new user |  X  |   |
| View private data* |  X  |  |
| Pin surveys to homepage |  X  |   |
| Send out `Call for Submissions` |  X  |  |

**Users can see the private data that they submitted, but not private data submitted by others*