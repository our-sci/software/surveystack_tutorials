# Respond to a Survey

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

There are multiple ways to respond to a SurveyStack survey:
1. [Receive an invitation to submit a response](#receive-survey-invitation) to a survey.  
2. Shared survey link.  
3. [Respond to a survey from your group workspace](#from-group-workspace).  

Here are some helpful tips for [filling out a survey](#filling-out-a-survey), [saving a draft](#save-a-draft) and [submitting a response](#submit-response).

### Receive Survey Invitation
Click on the survey link and get started.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/Survey_invitation.jpg)

### From group workspace
To access surveys from your group's workspace: 
1. Navigate to your group workspace using the dropdown in the top left of the screen.  
2. Select the survey from the sidebar menu (if visible) or select `All Surveys` from the sidebar to bring up all the groups' surveys.
3. Search for the survey you want to respond to.  
4. To start a survey, click the three dot menu and select `Start Survey`.   

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/survey_three_dots_menu.PNG)


### Fill out a survey
- Follow the prompts to navigate through the survey and answer questions.  
- Some questions are **Private**, they will have a blue eye icon (`This answer is private`). The answers to these questions are only visible to the person submitting the survey or the group administrator.  
- Some questions are **Required**, they will have a red asterisk (`Answer required`). You will not be able to select the `NEXT` button and advance through the survey until you answer the question.  

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/private_question_example.PNG)  
![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/required_question_example.PNG)
  
- To answer dropdown questions, either select the down arrow in the answer box to open and scroll through the answer list *OR* start typing in the answer box and the app will auto-suggest responses.  

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/dropdown_screenshot.jpg)

- You can pick the location by selecting your location *OR* by moving the map so that the red pin in the center of the map is over the correct location.  
     - If the red pin and your location do not match, you can select the `target` icon in the upper right corner of the map.   
- Once you have the red pin over the correct location, select the `PICK` button below the map.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/map_question_overview.PNG)

- To answer matrix questions  
     - Select `Add row` and enter/select values for each column.  
     - You can also delete and duplicate rows using the icons on the right side of the row.  

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Complete%20a%20survey/matrix_questions.png)

### Save a draft
If you exit the survey before completing the response, a `Draft` will automatically be saved and available to be completed later from the **My Draft Responses** page.

### Review a survey
- After you have completed the survey, the *survey outline* screen will give you the opportunity to review your answers before submitting the response.  
     - green checkmarks mean you answered the question  
     - an empty circle means that you did not answer the question  
     - an orange circle with an exclamation point means that you did not answer a *required* question.  

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/review_a_survey_image.PNG)
- You can also access the *survey outline* from any question screen by selecting the *outline* icon in the upper right corner of the question screen.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/survey_outline_button.PNG)

### Submit response
Once you are satisfied that the survey was completed correctly, select `SUBMIT` to submit the response to the database. Please make sure you are logged into your account before submitting. This will make it easier to track down responses in the future.  

- **Online Response**: If you have internet connection, you should get a green message: `Successful Submission`. You have the option to download your response as a pdf or to email a copy of your reponse using the `Download Submission` and `Email Submission` links.

- **Offline Response**: If you are **offline** you will get a red error message: `Offline Cannot submit without internet connection`. Select `OK` and your response will be **saved to your device as a draft**. *The response will be automatically submitted to the database the next time you open the app while connected to the internet.*

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/submit_response.jpg)

