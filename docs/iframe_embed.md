<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

### Embedding SurveyStack

SurveyStack can be embedded in an iFrame.  This is most useful to embed a survey into another website or application usually to be filled out by non-logged in users.

SurveyStack.io can be embedded using the normal URL, however, a leaner version of the site is available by adding the `?minimal_ui=true` query parameter.  This removes the top bar and side bar.

#### Example

- Normal Survey link: https://app.surveystack.io/surveys/60d20b292f38fe0001916497
- iFrame Survey Link: https://app.surveystack.io/surveys/60d20b292f38fe0001916497?minimal_ui=true
