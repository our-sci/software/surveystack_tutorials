# Building with the Common Profile

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>


## What is the Common Profile?

The [Common Profile](https://app.surveystack.io/surveys/60d20b292f38fe0001916497) is a question set for enrolling farmers. It contains a set of core questions about identity, location, land, practice, interests, and goals that many groups need to ask for onboarding into a program or project, such as the [OpenTEAM hub network](https://openteam.community/hub-network-program/) or [Pasa’s farmer research partnerships](https://pasafarming.org/programs/farm-based-research/). It is one of the most widely-used question sets that SurveyStack hosts, with numerous groups, including the openTEAM Fellows, using it for similar purposes. As of Spring 2023, it has nearly 400 entries.  

### How is it different from other SurveyStack question sets?
The Common Profile uses a wide range of shared questions co-created from Collabathon participants, and is our primary test set for trying out new interoperability features and integrations for these stakeholders and others. As a result, it’s bulkier than a question set designed for a more specific purpose, and has some legacy quirks folded in. It’s also collaboratively managed by many groups with different use cases, so updates and fixes require ongoing communication to make sure they’re fitting all the different ways this set gets used.    

On a less technical front, this question set is also a place where we can experiment as a community with different methods of co-design, and work toward a bigger picture of interoperability and collaboration. Even though each organization has their own priorities, by thinking through our needs as a whole “we can come together to support one another’s work and move the collective mission forward”, as OpenTEAM Fellow Jeanne Lurvey observed.  

### What are the differences between old and new versions? 

As we’ve developed this tool and implemented it across many different surveys, we’ve rapidly updated its functionality to fix bugs and add additional features. To see what version of the question set you are using, or review updates over time, enter the edit menu in your survey and select the library icon.  

![Library survey](https://gitlab.com/our-sci/resources/-/raw/master/images/Blog%20Post%20Images/common%20profile/library%20survey.png)

From there, you can search the Question Library for the Common Profile, and scroll down until you see the ‘Updates’ section. 

![Question Library](https://gitlab.com/our-sci/resources/-/raw/master/images/Blog%20Post%20Images/common%20profile/question_library.png)

If you are using an outdated version of the question set, you’ll see a notification when you enter the edit screen. 

![Outdated Question Set](https://gitlab.com/our-sci/resources/-/raw/master/images/Blog%20Post%20Images/common%20profile/outdated_qs.png)

From here, you can select the Version button highlighted in yellow and review what changes have been made since your last update. 

[Here is a video walkthrough for updating a question set that uses the Common Profile](https://www.youtube.com/watch?v=qy6zVnJxL6w):

<iframe width="560" height="315" src="https://www.youtube.com/embed/qy6zVnJxL6w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


<br>


### Survey Version and Question Set Version

**Note**: In the Results page for your survey, you can see which *survey* version was used for each submission by selecting the ‘show metadata’ slider:

![Survey Version](https://gitlab.com/our-sci/resources/-/raw/master/images/Blog%20Post%20Images/common%20profile/meta.survey.id.png)

If you’d like to see which version of the *Common Profile* was used for a given entry, you can do that by going to the [Results page for the question set itself](https://app.surveystack.io/submissions?survey=60d20b292f38fe0001916497). 

![QS Version](https://gitlab.com/our-sci/resources/-/raw/master/images/Blog%20Post%20Images/common%20profile/meta.survey.id2.png)

## Best Practices 

- Export a version of your survey before you make major changes. You can then test your changes by importing a copy of your survey into the builder, and also have a backup file if you need it. 

![Export](https://gitlab.com/our-sci/resources/-/raw/master/images/Blog%20Post%20Images/common%20profile/export.png)

- When setting the ‘required’ and ‘private’ settings in your survey, spend some time thinking about the unique needs of your users in the context of sharing with a broader community. 

> - At its core, the purpose of using the Common Profile is to share as much non-sensitive information about farms in the network as we can. This way, we are building a database of valuable information about what the farms in our networks look like, practice, and care about. We can then make better decisions, learn from each other, and advocate for shared interests. 
> - If your users don’t want to share this information, you have the option to make each of those questions private – but it may make more sense to build your own question set.

- Treat question sets as modular: rather than building nested question sets (a question set resource containing another question set resource) and adding them to a new survey, add each one individually. This helps ensure that dependencies and updates work correctly. 

### When editing the Question Set: 

**What can I change without reservations?** 

- Any additional new questions or content in your survey, outside of the common question set. For example, you could add a new question at the end of your survey about whether or not a farm in your network participates in 4-H activities. 

- Hints and more info - You can add as much information to these as is useful to your community. For example, if your group is focused on expanding definitions of leadership, you could add more information to the question “How much farming experience (in years) does the farm's leadership have?” based on your shared understanding. 

- Data *labels*, so long as the content of the question remains the same. The data label is the human-readable text of a question. For instance, you could change the label of the ‘system’ question to read “How do you keep dairy management records?” if your users are only dairy farmers. 

![Properties](https://gitlab.com/our-sci/resources/-/raw/master/images/Blog%20Post%20Images/common%20profile/properties.png)

**What can I change, but that may have downstream effects?**

- Adding questions within the body of the common question set - You can do this, and it often makes sense for user flow, but updates to the Common Profile may impact these questions, and they are not shared with others.  

![Years Farming](https://gitlab.com/our-sci/resources/-/raw/master/images/Blog%20Post%20Images/common%20profile/years_farming.png)

For example, Pasa has added a question to the ‘other’ section of the Common Profile asking “When was your first year farming?” Responses to this question will show up in the ‘Results’ page for their survey, but not for users interacting with the Common Profile through another survey. 

- You can delete entries to shared lists that allow modification, but may lose comparability or interoperability with groups that need this information. For example, you have the ability to delete options from the “interests” list, and if your group does not sell their products, you could remove the option “Increasing sales”. 

![Interests](https://gitlab.com/our-sci/resources/-/raw/master/images/Blog%20Post%20Images/common%20profile/interests.png)

However, the best practice is to keep all list options available, and perhaps to add a hint to this question explaining “You can ignore options such as sales that aren’t relevant to you.” This gives your members the ability to see what the potential list of options are for everyone using the Common Profile, and doesn’t skew the outcomes of this question in the ‘Results’ page. 

**What should I not change even if I technically can?**

- Don’t add to list resources that are referenced in the API. For instance, if you add an entry to a list that doesn’t exist in the API schema and a user selects that entry, they will receive this error message: **“API Compose error - common_profile.common types/0 must match a schema in anyOf {}”** While current versions of the Common Profile should have all API reference questions locked, older versions did not. 

- Don’t reword questions to substantially alter what’s being asked.  For instance, don’t change a question asking ”What animals do you have?” to instead read “What books do you like to read?” Your user’s answers will still appear under the ‘animals’ column in the results page, and will make the results of that column unuseable. There isn’t a way for us to hard-code this distinction into the form itself, so we’re counting on survey builders like you to use your best judgment when rewording. If you aren’t sure about the way you’ve reworded a question, or would like community support, the [GOAT forum](https://forum.goatech.org/) or the [OpenTEAM Feedback group on Hylo](https://www.hylo.com/groups/openteam-tech-toolkit-group) are both great places to start.

- Don’t delete & re-add questions to make small changes, such as capitalization or word order – instead, see if you can alter your question to work with the common resource, or propose an update to the broader group. 

If you are interested in using the Common Profile, or have feedback for our next Collabathon, we would love to hear from you! Contact vic@our-sci.net to get in touch. 


