# The FarmOS Enrollment Process

This covers how producers, project managers, assessors, data intermediaries, agronomists or other people supporting groups of producers can enroll farmers into their programs using **FarmOS instances** to store the farm data and the **Common Profile** to get basic data and connect to additional opportunities (models, loans, govt. programs) more easily in the future.

***Please note that if you are a producer and your farmOS instance is connected to your profile as part of a surveystack group, any admin of the group(s) you are part of will have access to your farmOS instance. They will have the ability to create or modify assets or logs in your farmOS instance.*** 

## Producers  
As a SurveyStack user you have the ability to link your FarmOS account to your SurveyStack profile. This feature allows the information you are submitting through SurveyStack to automatically flow into your farmOS account which then can be used in other tools such as the Farmers Coffee Shop.  

### Connecting your existing farmOS account  
To link your existing farmOS account to your SurveyStack profile, email **support@farmier.com** and request that your farmOS account get added to SurveyStack and include which group you would like to add your account to. Note - you must add your account to a group in order to have your farmOS account added to SurveyStack.   

### Creating a new farmOS account in SS  
Only group admins can create new farmOS instances in SurveyStack. This is due to the payment structure, there is a monthly fee for hosting the farmOS instance that your group will be paying for. If you would like to pay for your own farmOS instance and then link it to your SurveyStack profile go to farmier.com and once your farmOS account is created, follow the instructions on connecting your existing farmOS account to SurveyStack.  

### Adding and removing users from your farmOS account  
When logged in to SurveyStack click on the profile icon on the top right of the screen and select FarmOS Profile. On this page you will see your farmOS account, the groups that your account belongs to and the users who have access to submit surveys and push data into your account. To remove a user from your account you can simply push on the ‘x’ next to their name. A pop-up will appear asking you to confirm this action. A crown symbol will appear next to the name of any users that are considered an ‘owner’ of your farmOS account. This allows them to manage which users and groups have access to the farmOS account.

To add a user to your account so they may have access to submit surveys and push data to your farmOS account click on the ‘add button’ underneath your farmOS account name. A screen will pop-up asking for the email of the person you would like to add, this person must already be a SurveyStack user to be added here. You can also select if you would like this person to be an owner by selecting the box that says ‘this person will be an owner too’.  If you would like to add someone who is not currently in SurveyStack reach out to your group admin to add the user to SurveryStack first. 

![farmOS_profile](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/FarmOS%20Admin%20features/edit%20your%20profile.png)

### Re-assign farmOS ownership  
When logged in to SurveyStack click on the profile icon on the top right of the screen and select FarmOS Profile. In this page you will see your farmOS account. Below your farmOS account url you will see a button labeled ‘re-assign’ (see above photo), this button will allow you to designate someone else as the owner over this farmOS account, removing yourself as the owner.  This will not change your admin settings inside the actual farmOS account, this setting determines which SurveyStack user the farmOS account is attached to inside of SurveyStack. The farmOS account will follow the owner when they join or leave a group inside of SurveyStack without having to manually add or remove the farmOS account from a group.  

### Remove farmOS account from a group  
When logged in to SurveyStack click on the profile image on the top right of the screen and select FarmOS Profile. In this page you will see your farmOS account. To remove your farmOS account from a group simply click on the ‘x’ to the right of the group name under the column ‘Groups with access’.  


## Group Admin  
As a group admin you can manage what FarmOS accounts are mapped to users in your group.  

### Create a farmOS account for a user  
To create a farmOS instance for a user go to your group page by clicking on ‘Group’ in the menu on the left hand side then search for your group in the list. Once you have selected your group and are on the group page click on the ‘Admin’ button on the top right of the screen. Then scroll down on this page to the ‘Integrations’ section and click on ‘FarmOS’. 

![farmOS_integration](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/FarmOS%20Admin%20features/group%20integrations.png)

Use the search bar to look for the user you want to make a FarmOS account for. Click on ‘Connect Farm’ and then click ‘Create Farm’.  

![connect_farm](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/FarmOS%20Admin%20features/connect%20farm.png)

Select the appropriate plan, if there is more than one, then enter the Instance URL, in all lowercase. Generally the url is just the farm name, all lower case with no species. For example: Happy Days Farm would be happydaysfarm. Once the url has been entered, click ‘Check URL’ to ensure this url does not already exist. If it doesn’t then continue completing the form and click ‘Register FarmOS Instance’. The instance is now created and connected to the user. Your user will receive an email about their new FarmOS account with instructions on setting up a password and a guide on how to use FarmOS.  

![create_farm](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/FarmOS%20Admin%20features/create%20new%20farm.png)

If the url you are trying to create is already created you can either check with your user to see if they already have a FarmOS instance or check with a Super Admin to see if we already have this instance in our database. 

### Adding an existing SS user with a FarmOS account to your group  
There are two steps to adding an existing SurveyStack user with a FarmOS account to your group, first you will add the user to your group, then you will add their farmOS account. 

First, go to your group page by clicking on ‘Group’ in the menu on the left hand side then search for your group in the list. Once you have selected your group and are on the group page click on the ‘Admin’ button on the top right of the screen. There you will see the list of members for your group. Select ‘New’ and add the user's email address and click ‘Add Member’. The user is now a member of your group and they have received an email letting them know they have been added to your group. 

Second, scroll down on the admin page to the ‘Integrations’ section and click on ‘FarmOS’. Use the search bar to look for the user you just added and click ‘Connect Farm’.  Click on ‘Select Farms’ and choose the user's farm from the dropdown list. If the user's farmOS account is not in the list, reach out to a Super Admin for support.

### Connecting an existing FarmOS account to a user in your group  
Before you can add an existing farmOS account that is not in SurveyStack to a user in your group, the user must first email support@farmier.com and request to have their farmOS account added to SurveyStack and into your group. 

Once the instance has been added to SurveyStack and your group you can then go to your group page by clicking on ‘Group’ in the menu on the left hand side then search for your group in the list. Once you have selected your group and are on the group page click on the ‘Admin’ button on the top right of the screen. Scroll down on the admin page to the ‘Integrations’ section and click on ‘FarmOS’. Use the search bar to look for the user and click ‘Connect Farm’.  Click on ‘Select Farms’ and choose the user's farm from the dropdown list. If the user's farmOS account is not in the list, reach out to a Super Admin for support. 

More than one user can be connected to a FarmOS account as requested by the account owner so the other person may then submit surveys for this account. Only Super Admins and Users can connect additional users to a farmOS account at this time, reach out to a Super Admin if you have a user that would like to do this.

### Removing a User and a FarmOS account from your group  
To remove a user from your group go to your group page by clicking on ‘Group’ in the menu on the left hand side then search for your group in the list. Once you have selected your group and are on the group page click on the ‘Admin’ button on the top right of the screen. A page will appear with a list of users. Search for the member you would like to delete and click on their name. This will bring you the ‘Edit Membership’ page, select Delete in the top right hand corner. A pop up will appear asking if you are sure you want to delete this member. Select ‘Delete’ to delete the member from the group. This member is now removed from your group and their FarmOS account is also automatically removed from your group as long as it is not connected to any other user.  

### Removing only a FarmOS account from your group  
To remove only a farmOS account (not the associated user) from your group go to your group page by clicking on ‘Group’ in the menu on the left hand side then search for your group in the list. Once you have selected your group and are on the group page click on the ‘Admin’ button on the top right of the screen. Scroll down on the admin page to the ‘Integrations’ section and click on ‘FarmOS’. Use the search bar to look for the user and click ‘manage’ to the right of the farmOS account name. De-selects all of the groups and click ‘Update Groups’. Select why the farmOS instance (account) is being removed and click ‘Submit’ or click ‘Don’t add note’.  

![manage_group](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/FarmOS%20Admin%20features/manage%20groups.png)

### Moving farmOS instances between subgroups  
To remove only a farmOS account (not the associated user) from your group go to your group page by clicking on ‘Group’ in the menu on the left hand side then search for your group in the list. Once you have selected your group and are on the group page click on the ‘Admin’ button on the top right of the screen. Scroll down on the admin page to the ‘Integrations’ section and click on ‘FarmOS’. Use the search bar to look for the user and click ‘manage’ to the right of the farmOS account name. Use the drop-down menu to select which group(s) the instance should be part of.




