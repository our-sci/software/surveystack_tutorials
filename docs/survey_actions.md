# Survey Actions

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

The survey actions available to you are visible in the 3-dot menu and based on your role within the group.

|Option|Role|Description|
|-------------|--------|------------------------------------|
|Start Survey |Member / Admin|Start a new response to a survey|
|Start Survey as Member|Admin|Start a new response for a member, the member will own the response|
|[Call for responses](https://our-sci.gitlab.io/software/surveystack_tutorials/manage_a_group/#call-for-responses) |Admin|Send survey to members with a magic link to log members in to SurveyStack|
|Description|Member / Admin|View description of the survey|
|Print Blank Survey|Member / Admin|Download a PDF version of the blank survey|
|Edit|Admin|Opens the survey in the builder so it can be edited|
|[View Results](#view-results)|Member /Admin|[View](#view-results), [filter](#filter-results) and [download](#download-results) survey responses|

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/survey_actions.jpg)

### View Results
There are three ways to view results:

- **List:** List of survey responses based on who submitted the response and when. No response data is visible.
- **Table:** Survey response data viewable in a table. Only data you have permission to see will be visible, with private data hidden from non-admins. 
- **Raw:** Survey responses visible as a json.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/view_results.jpg)

### Download Results
- Select `EXPORT RESULTS` from the upper right of the results page. 
- There are numerous download settings available:
    - **Format:** Results can be downloaded as a csv or json file.
    - **Range:** You can also choose to download all of the data or just a subset of the data based on filters or just downloading the active page.
    - **Matrix Answers:** Matrix question types have a complex results structure that doesn't easily map to a spreadsheet. They are essentially a table within a single cell. For this reason, when downloading survey results, you have several options depending on how you need to deal with your data (see special note below).
- After choosing your download settings, select `DOWNLOAD`.
- You can also copy the **API link** which can be used to integrate SurveyStack data into a data pipeline or acquire the data in a more flexible way.
- **SPECIAL NOTE FOR MATRIX QUESTION TYPES:** The export results menu has multiple options relevant to matrices.
    - Selecting **add a row for each matrix answer** from `Matrix answers` will simplify the data into the flattest possible spreadsheet (only available for the `CSV` format. This will expand each row containing a matrix into several rows, instead of 1 submission = 1 row, and you will get a table that's easy to work with.
    - Selecting **Keep matrix answers in a single cell** from `Matrix answers` will encode the matrices as json text inside single cells, while the rest of the data is translated to the CSV in a straightforward way.
    - If you want maximum flexibility, you can get a `JSON` file in the exact format SurveyStack uses for storage, which you can process in any programming language/data environment you intend to use as needed.

![Image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/Download_results.jpg)

### Filter Results
1. Open the `Filter settings` be selecting the filter icon to the right of the search bar.
2. Choose which **Field** (column header) you want to filter by.
3. Decide which filter to use (see options below).
4. Enter the `Value` that you are searching for.
5. Select `Apply`


![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/filter_results.jpg)
