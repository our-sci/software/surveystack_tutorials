# Create a New Survey

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

Go to [surveystack.io](https://app.surveystack.io) and open up the sidebar using the menu icon in the upper left corner.  
1. Select `Builder` from the menu.  

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/Create%20a%20new%20survey/updating%20photos1.png)  

2\. Give your survey a name.  
3\. Add survey details to your survey.  
4\. Add survey resources.  
5\. The three dot icon has several other important features. You can export/import surveys as json objects, manage survey versions, add the survey to the question set library, etc. You can also adjust print settings here, with options to show instructions questions and/or show unanswered questions. These print settings are applied when a user prints a copy of their submission at the end of filling out a survey. 

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/Create%20a%20new%20survey/builder_tour2.png "Screenshot of the survey builder with arrows to survey name, description and resources.") 

### Survey Details
1. Assign survey to group - survey submissions will automatically be assigned to this group if the user is not signed in or has not chosen an active group. Otherwise, the submission will be assigned to the `active group` chosen by the user.
2. Select who is able to submit responses  
  >>- Everyone - Anyone with the link to the survey can submit a response.  
  >>- Logged in Users - Anyone with a SurveyStack account can submit a response.  
  >>- Group Members - Anyone who has accepted an invitation to be part of your group can submit a response.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/Create%20a%20new%20survey/survey_details3.png)  

3\. Add a description of your survey, which will be visible from the survey’s landing page. On the landing page there is also an option to 'print blank survey'. This will download a pdf version of the survey can then be passed around and reviewed as you’re building a survey with a group, or handed out as paper surveys to fill out in the field–and all the options you’ve built into your SurveyStack survey are captured.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/Create%20a%20new%20survey/survey_description4.png) 

### Add Resources
You can add ontology lists at the beginning of your survey for use in [dropdown questions](#dropdown-questions), or add them when creating dropdown questions.  
1. Select `+ Create Ontology`.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/Create%20a%20new%20survey/resources5.png)  

2\. Add **List Label**: Name that will be visible in the dropdown list.  
3\. Add **List Data Name**: Name of the resource that will be accessible within the survey.  
4\. Create the list row by row or add rows to an existing list.  
5\. **Actions**: You can move, duplicate or delete items using the action buttons.  
6\. Upload a csv file to create a list.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Create%20new%20survey/upload_csv.png) 