## Group Functions

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

|Actions or Permissions|Admin|
|---------------------|-----|
|[Pinned Surveys](#pinned-surveys)|Pinning surveys within a group adds them to the app’s homescreen|
|[Manage Members](#manage-members)|List of group members. Group Administrators have the ability to add/delete <br> members, connect their accounts to FarmOS and define a users permissions.|
|[Call for Submissions](#call-for-submissions)|Send magic links to group members so they are automatically logged into their <br> account when they open up the survey|


### Pinned Surveys
To pin a new survey to the homescreen:  
1. Select `New..` from the groups homepage.  
2. Select the survey that you wish to pin to the homescreen from the list.

To unpin a survey from the homescreen, click on the `trash` icon in the Survey box.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Group%20Features/pin_surveys.png)

### Manage Members
#### Adding new members
1. Select `NEW..` from the member section of the Group Editor.  
2. Define the members role:
     - **User:** Can fill out surveys and see their own private data
     - **Admin:** Can create surveys and scripts, add new users, see private data, etc. For complete list of admin features check out the [Administrative Rights table](#administrative-rights)
3. Enter the email to send the invitation to.
4. Choose whether to send the invitation right away or to wait.
5. Click `SUBMIT`

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Group%20Features/invite_to_group.png)

#### Checking member status
From the Group Editor you can check the status of members and invitations:
- If the text for the member is black, that means they accepted their invitation are created a SurveyStack account.
- If there is a `crown` icon to the right of the user, they have admin rights in that group.
- If the invitation has not been accepted, it will say `[Pending]` before the member and there will be a timestamp of when the invitation was sent, or a message stating the the invitation was not sent.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Group%20Features/user_admin.png)

#### Edit a member
To edit a membership, click on the users name from the Group Editor. From the `edit membership` screen you can:  
- Change a users role  
- Delete the user from the group  
- Edit a `Membership integration` - the only existing membership integration is integrating a FarmOS account with a users SurveyStack account.  
- Add a new `Membership Integration` - connect a FarmOS account to the users SurveyStack account.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Group%20Features/member_integration.png)

### Administrative Rights
| Actions or Permissions | Admin | User |
|---------------------|-----|----|
| Fill out surveys and submit data |  X  |  X  |
| Create or edit a survey |  X  |   |
| Create or edit a script |  X  |  |
| Invite a new user |  X  |   |
| View private data* |  X  |  |
| Pin surveys to homepage |  X  |   |
| Send out `Call for Submissions` |  X  |  |

**Users can see the private data that they submitted, but not private data submitted by others*


### Call for submissions
Using this feature, you can send users a `magic link` to a survey that will automatically log them in when they open it. This is really helpful when users may be using multiple devices to complete surveys (e.g. computer and mobile device).

To create a new call for submissions:  
1. Click on the `CALL FOR SUBMISSIONS...` icon in the upper right hand corner of the Group Editor.  
2. Click `SELECT SURVEY` and search or scroll through the list of surveys to choose the survey that you want your collaborators to complete.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Group%20Features/call_for_submissions.png)

3\. You will then have the option to edit the Subject and Message of the email that your collaborators will receive. 

**Important Note:** Do not delete `%CFS_MAGIC_LINK` from the message or the link will not automatically log users in any more. 

4\. Select all of the users that you want to receive the email. **You can only send them a magic link if they have accepted their SurveyStack invitation.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Group%20Features/select_members.png)

5\. You will then have the option to edit the Subject and Message of the email that your collaborators will receive. 

**Important Note:** Do not delete `%CFS_MAGIC_LINK` from the message or the link will not automatically log users in any more. 

6\. Select all of the users that you want to receive the email. **You can only send them a magic link if they have accepted their SurveyStack invitation.
