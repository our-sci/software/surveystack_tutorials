# Download the App

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

SurveyStack is a *progressive web app* which means you won't find it in the apple or google play store, but you can **Add to Home screen** and it will function in the same way as an app. 

## When to add SurveyStack to your homescreen?
- **Mobile devices:** SurveyStack behaves like an app and is much easier to navigate when downloaded to phones and tablets
- **Offline Use:** If using SurveyStack when offline, or with limited connectivity, downloading the app will make surveys, scripts and other resources fully available offline. Survey responses will be saved locally on the device until it is connected to the internet again, then they will automatically get uploaded to SurveyStack.

## Adding SurveyStack to your homescreen.
SurveyStack works best on the following internet browsers:

- [Chrome](#chrome)
- [Firefox](#firefox)
- [Safari](#safari)

A quick download shortcut will appear the first time you visit app.surveystack.io. Follow the prompts to add the app to your homscreen. If the quick download shortcut does not appear, follow the steps below (by browser) to add the app to your homescreen.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/Quick_download_guide.jpg)

#### Chrome
1. Select the three dots at the top of the browser
2. Select `Add to Home screen`
3. Select `Install` 

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/Chrome_install.jpg)

#### Firefox
1. Select the three dots at the bottom of the browser
2. Select `Install`
3. Select `Add`

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/Firefox_install.jpg)

#### Safari
1. Select the share options button on the bottom of the screen
2. Select `Add to Home Screen`
3. Select `Add`

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/Safari_install.jpg)
