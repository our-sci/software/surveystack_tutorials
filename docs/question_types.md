# Question Types

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

After selecting the `plus` icon, choose the question/object type to add.

|Question Type|Description|
| :-------------: |:----------------------------|
|[Seach Question Library](https://our-sci.gitlab.io/software/surveystack_tutorials/QSL/)|Search for question sets in the the library|
|[Group](#question-groups)|Group questions together in the survey|
|[Page](#question-pages)|Put multiple questions on one page| 
|[Instructions](#instructions)|Provide text instructions for data collectors in the field|
|[Instructions Split](#instructions-split)|Provide text and image instructions for data collectors in the field|
|Text|Type in the answer in short answer form|
|Number|Enter a number|
|[Date](#date-question)|Enter date, can format as individual date, week, month or year|
|[Location](#location)|Uses the devices GPS to capture location data|
|[Multiple Choice](#multiple-choice-and-checkbox)|Provide a list of possible answers, user can select only 1 answer|
|[Checkbox](#multiple-choice-and-checkbox)|Provide a list of possible answers, user can select many answers|
|[Dropdown](#dropdown)| Possible answers are imported as a csv, for longer lists or reference another question/survey to create your list.|
|[Matrix](#matrix)|Create a table with custom columns that allows users to add rows|  
|[Image](#image-upload)|Allow users to upload images|
|[File](#file-upload)|Allow users to upload files|
|[Script](#scripts)|Run javascript in the survey|
|[Map](#map)|Create areas, fields, or points|
|[FarmOS_Field](#farmos-field-and-planting-questions)|Pulls area’s from a Farmers FarmOS account, as long as the farm is included in an <br> aggregator and the SurveyStack and farmos account are linked|
|[FarmOS_Planting](#farmos_field-and-planting-questions)|Pulls planting assets from a Farmers FarmOS account, as long as the farm is <br> included in an aggregator and the SurveyStack and farmos account are linked|
|FarmOS Farm|Pulls FarmOS farms that are linked to that account and are in the aggregator|
|FarmOS UUID|Create a unique FarmOS Field or Planting ID|

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/Question_options.jpg)

### Question Groups

Questions can be added to groups. Using groups can make it easier to use survey logic (relevance) and to copy groups of questions. This will not be visible to the user.

Add questions to a group by either:  
1. Highlighting the Group in the `Survey Outline` and then selecting a new question (below left), OR  
2. Clicking on the left side of an existing question box and dragging it into the group (below right)

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/add_to_group.png)

### Question Pages  

Questions can be added to pages so the user can see multiple questions at a time. This is often used when you have a group of related questions such as address or contact information. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/page.jpg "user view of pages")

Add questions to a page by either:  
1. Highlighting the Page in the Survey Outline and then adding a new question (blue plus button on bottom left), OR  
2. Clicking the left side of an existing question box and dragging it into the page.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/add_to_page.png)

The spaces between the questions on the page can be removed by selecting `compact`.

### Instructions   

Provide text instructions for data collectors in the field. Currently, users have some basic functionality to add headers, numbered or bullet lists and text formatting. You can also add links to this question type by:  
1. Write the text that you want for the link in the body of the instructions question (Example: "Watch this video for more information...")  
2. Copy the link that you want to include in the directions  
3. Highlight the text that you want linked (Example: "video"). An "Add Link" pop-up will appear above the highlighted text.  
4. Copy the link into the pop-up box and select the check icon.  
When the user clicks the link while taking the survey the link will open in a new tab in their browser.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/instructions.png)

### Instructions Split

Provide text and photo instructions for users. 

Add a resource (image):
Currently you must store resources somewhere online that you can link them because they are added by URL. The best place to do this is gitlab because the URL won't change. This also means that you must be online to see the images in SurveyStack. We are developing resources to store images so they will be available offline and that will be coming soon. 

1. Click the `Choose a Resource` dropdown and click `+ new`.
2. Add an image label, data name, and URL and then select `UPDATE`.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/instructions_split.png)

Adding images with google photos:
- Select the image you want to upload.
- Right click on the image and select 'Copy Image Address'.
- Use `ctrl` + `v` (windows pc) or `Command`+ `v` (Mac) to paste the image URL into the appropriate spot as shown above.

### Text and Number

Users fill in text or number boxes. You can now allow users to scan QR codes with phones for quicker and more accurate entry on text questions. 

### Location

Allows users to select the location of interest using:  
1. Using the device’s GPS to capture the coordinates and accuracy data (below left) OR  
2. Using Mapbox interactive mapping tools, by moving the map so the location of interest is in the center of the map (below right).

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/location.png)

### Date Question

**Type**: Users can select from 4 date types:
- Full Date (Month, Day, and Year)
- Month and Year Only
- Week of Month and Year
- Year Only

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/date.png)

### Multiple Choice and Checkbox

These question types are best used for selecting one answer (multiple choice) or multiple answers (checkbox) when the list of possible answers is fairly short (10 or less answers). 

To add answer options:  
1. Select `Add options` under resources  
2. Click `+ ADD Row`
3. Add in each option you want users to choose from. **Label**: is the text that the user will see when filling out the survey. **Value**: is how the answer will be stored in the database and output in the results. Avoid using spaces or long answers for Value.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/multiple%20choice%20questions.png)

4\. **Allow Custom Entry**: When checked, users are able to type in a custom entry that is not present in the list of options (below left).  
5\. **Default Value**: Select a value from the options you've entered to pre-fill the question with. The user can change this value if necessary.
6\. **Change the order of options**: Click on the area surrounding the `Label` and `Value` and pull the box up or down to change the position in the option list.
7\. **DELETE**: Click on the trash icon to delete an option.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/custom_entry.png)

### Dropdown

Dropdown questions are best used when you have a long list of options that a user might select from. For example a crop or fertilizer list. 

To select an existing list or add a new list:  
1. Go to the `Resources` dropdown and select the appropriate option.  
2. If you are adding a new ontology list a pop up will appear and you can add you list either row by row or by uploading a csv.  
3. Be sure to edit the list label and list data name so that you can use this resource in other parts of your survey.  

Once you’ve added or selected your resource look through the other options on the question to decide how users will fill this question in. Selecting `Autocomplete` means that as the user starts typing in a value the list will narrow down automatically. `Allow custom answer` means that users can input values that are not on the list. To avoid duplicates like kale and Kale or different spellings of bok choy, `Autocomplete` must be checked when `Allow custom answer` is selected. You can also allow users to choose more than one answer by checking `Multiple select`. 

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/dropdown%20questions.png)

Users can also create a survey reference that take answers from a previously submitted answers to the referenced question and put them in a dropdown list for the user to select from. You can create a reference within your own survey or choose a question from a different survey. The reference feature 

To create a survey reference:
1. `+New Survey Reference`: select to browse surveys and questions to create a reference.
2. **Select survey**: select the survey that contains the question you want to reference.
3. **Select question**: select the question you want to reference.

![survey_reference](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/Survey_reference.jpg)

Users choose answers by either selecting them from the dropdown menu or by typing in the auto-select text box.

### Matrix

Matrix questions allow users to add rows of information in a table like format where the survey creator has made columns for the data they need to collect. These questions are best used to organize data for a set of questions relating to a topic. For example if you are trying to gather a farmer's tillage/soil disturbance data from multiple different fields.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/user_matrix.png)

**To create columns:**  
1. Scroll down and select open editor. Two example columns will pop up.  
2. Each column can be a different question type (dropdown, text, number, date, Farmos field, Farmos planting).
   >>- To add a csv to dropdown questions click the + next to 'Resource' or select from existing lists using the down arrow. Then follow instructions above for [adding a csv](#dropdown-questions) to a survey.  
3\. Use the plus button or the + add column button on the top right to create additional columns.  
4\. Select a question type and enter a label and value for each column. Additionally select whether you want the column to be private or not.  
5\. Drag or use the arrows at the top left of each column to change the order the columns appear in.  
6\. When you are done creating columns click the close button at the bottom of the column editor.  
7\. Under the editor there is a spot to 'Add row label'; you can change this to anything you want the user to see such as 'Add planting' or 'Add event'. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/matrix_editor.png)

### Image Upload
Image upload allows the user to upload one or more images from their computer. There are many different applications for this question type. It can be helpful if you want to see pictures of a farmer's field or soil or even for collecting photos for a website.

In the builder you have the option to allow users to submit multiple files. Accepted file types include .gif, .png, .jpg, and .jpeg. After uploading a file the user can remove or rename the file as necessary.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/picture_upload.png)

For mobile users there is also the option to use the camera to add photos.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/take_picture.PNG)

If this field is left public then users will be able to download images from the results page.

### File Upload
File upload allows the user to upload one or more files from their computer. This can be useful if you want to collect a farmer's soil health management plan or give the user the option to upload lab results.

Accepted file types include .pdf, .csv, .gif, .png, .jpg, .jpeg, and .txt. The survey creator has the ability to restrict allowed file types and, similar to the image upload question type, the creator can allow users to submit multiple files. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/file_upload.png)

### Map
Use this question to allow users to draw polygons, lines, or points on the map. 

**To draw a field**
1. Use the geolocate button or search for an address  
2. Select the Draw A Polygon icon (looks like a square)
3. Click on each corner of your field

![map_question](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/map.png)

### Scripts  
Use script questions to add custom javascript code to your survey. Using scripts you can reference anything in the survey's json object and provide feedback to the user in real time. 

Common types of scripts used include:
- **Hardware integration:** For example, the [Our Sci Reflectometer](https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/) is controlled by SurveyStack scripts, with the scripts sending instructions to the instrument and returning instrument outcomes to the user. 
- **Display calculations or outcomes to the user:** SurveyStack users have developed scoring metrics based on the answers to surveys that are calculated and displayed to users using scripts. Also, calculations from measurement protocols can be displayed.
- **Pull API data into your survey:** Scripts can be written to make API calls to fetch data (ex: weather data for an address).
- **Provide recommendations or predictions:** Provide detailed recommendations/predictions to users based on the answers to survey questions, hardware generated data, data pulled in via API or a combination of all three data sources.

To create a new script, select the **Scripts** option from the sidebar menu and then select `Add` or `Create a script`. This will open a scripting environment for entering the javascript code, make sure to give your script a name and to `Save` frequently. 

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/create_script.jpg)

### FarmOS Field and Planting Questions

SurveyStack and FarmOS have been developed to be interoperable so that data can be passed between both platforms. This allows project managers to use FarmOS's farmer-facing user interface to store and present data to farmers while also being able to use SurveyStack's flexible survey creation tools to structure data collection. The end result is an integration that improves data comparability between every farmer participating in a given project while maintaining farmers ownership and control over their data. 

FarmOS_Field and FarmOS_Planting questions pull area and asset data from a farmers FarmOS account into SurveyStack so that they can associate the activity from a survey to a specific field or planting. **These questions only work if the user has a `Membership Integration` connecting their FarmOS account to their SurveyStack account. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/farmOS.png)
