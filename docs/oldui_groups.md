# Group Information

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

### What is a Group?
A **Group** is a set of users (people or institutions) who are collaborating. By using groups, users can share surveys, add members and member integrations, and set permissions about who can create/edit surveys and who can view private data.

### Create a Group
To create a group:  
1. Select `Groups` from the side menu  
2. Select `NEW...` from the upper right corner.  
3. Enter the `Group` name (the `Slug` will be created automatically from the group name).  
4. Click `CREATE`.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Group%20Features/create_group.png)

### Edit a Group
To edit a group, click on the `Admin` button in the upper right hand corner of the group page to open the **Group Editor**. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Group%20Features/admin.png "screenshot of admin features available: pinning surveys, adding members, call for submissions, and group integrations.")


### Subgroups
A subgroup is basically a group within a group. It can help with organization if you have a large group or if you’re doing a lot of different things. 

A subgroup functions in the same way as a group. There are still admins, you can add people to a subgroup, and you can pin surveys (see [group functions](https://our-sci.gitlab.io/software/surveystack_tutorials/group_functions/)). 

Additionally, if you are an admin of a parent group you will automatically be an admin of any subgroups that are created under that parent group. However, each subgroup can have its own admins that are not admins of the parent group. 

Some situations where subgroups may be useful:
- You are conducting the same experiment in multiple countries and need the same survey in different languages. 
- Your group is running multiple different projects or experiments that do not overlap. For example Our-Sci has subgroups for Hardware Development, Question Sets, and Surveystack testing.

### Creating a subgroup
*You must be an admin to create a subgroup.
1. Select the hamburger menu and go to `groups`.  
2. Select a group that you are an admin of and select `new` on the subgroups box. 

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/subgroups.png)

3. Fill in the information for your subgroup. The slug is a URL friendly version of the subgroup name meaning no capitals and no spaces. 

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/create%20subgroup.png)

