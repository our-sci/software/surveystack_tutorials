# Manage a Group


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

|Action|Description|
|--|--|
|[Administrative Rights](#administrative-rights)|Which features are available to Admins and Members|
|[Manage Members](#adding-new-members)|Adding members to a group or changing their permissions.|
|[Subgroups](#creating-a-subgroup)|How to create and edit subgroups.|
|[Pin surveys](#pinned-surveys)|Coming soon.|
|[Call for Responses](#call-for-responses)|How to send a survey out to group members for completion.|
|[Add custom documentation](#custom-documentation)|Add links to group-specific documentation|

### Administrative Rights
Only group admins are able to manage groups. This table summarizes the actions that **Admins** and **Members** can take within a group workspace.

| Actions | Admin | Member |
|---------------------|-----|----|
| Respond to Surveys L |  X  |  X  |
| Create or edit a survey L |  X  |   |
| [Create or edit a script](https://our-sci.gitlab.io/software/surveystack_tutorials/question_types/#scripts) |  X  |  |
| [Add a new member](#adding-new-members) |  X  |   |
| View private data* |  X  |  |
| [Pin surveys]((#pinned-surveys)) |  X  |   |
| Send out [Call for Responses](#call-for-responses) |  X  |  |

**Users can see the private data that they submitted, but not private data submitted by others*

### Adding new members
1. Go to the workspace for the group you'd like to manage. 
2. Select `Members` from the sidebar menu.  
![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/member_managment_page.PNG)
2. Select `INVITE NEW MEMBERS` in the upper right of the screen.
3. Define the new members role:
     - **Member:** Can fill out surveys and see their own private data
     - **Admin:** Can manage the group based on the rights in [this table](#administrative-rights)
4. Enter the email address of the new member
5. Choose the best option for the new member:
     - **Add Member:** They will be immediately added to the group and will be sent an email informing them that they were added.
     - **Invite Member:** They will be sent an email invitation and will join the group once they have accepted the invite.
6. Click `ADD/INVITE MEMBER`


#### Checking member status
From the **Members** tab, you can check the status of members and invitations:
- If there is a `crown` icon to the right of the user, they have admin rights in that group (red box in the image, below).
- If the invitation has not been accepted, it will say `[Pending]` before the member and there will be a timestamp of when the invitation was sent, or a message stating the the invitation was not sent (yellow arrow in image, below).
- If there is no "pending" text, this means the member has accepted their invitation to the group. 

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/member_rolls.PNG)

#### Edit a member
To edit a membership, click on the users name in the **Members** tab and select `EDIT` from the 3 dot menu. From the `Edit Membership` screen you can:  
- Change the member role
- Delete the member from the group  


### Subgroups

A subgroup is basically a group within a group. It can help with organization if you have a large group or if you’re doing a lot of different things. 

A subgroup functions in the same way as a group. There are still admins, you can add people to a subgroup, and you can [pin surveys](#pinned-surveys).

If you are an admin of a parent group you will automatically be an admin of any subgroups that are created under that parent group. However, each subgroup can have its own admins that are _not_ admins of the parent group. 

Some situations where subgroups may be useful:
- You are conducting the same experiment in multiple countries and need the same survey in different languages. 
- Your group is running multiple different projects or experiments that do not overlap. 
  - For example Our-Sci has subgroups for Hardware Development, Question Sets, and Surveystack testing.

#### Creating a subgroup
*You must be an admin to create a subgroup.*
1. Navigate to the workspace of the parent group you wish to add a subgroup to.
2. Select `Settings` from the sidebar menu.
3. Select `NEW...` next to the Subgroup header. 
4. Fill in the information for your subgroup. The slug is a URL friendly version of the subgroup name meaning no capitals and no spaces. 
5. Click `CREATE`.

### Pinned Surveys

Coming soon.

<!-- *You must be an admin to create a subgroup.
1. Navigate to the group or subgroup you wish to pin a survey to.
  - Note: the survey must have been created in the parent group, you cannot pin surveys from other groups. 
2. Select `Settings` from the sidebar menu.
3. Select `NEW..` next to the `Pinned Surveys` header. 
4. Select the survey you wish to pin.
5. You will see the pinned survey show up in the sidebar menu. -->

### Call for responses
SurveyStack allows you to send an email to members of your group with a link to complete the survey.  

To call for responses:
1. Navigate to your group page.
2. Select the three dots next to the survey you wish to send.
3. Select `Call for Responses`.
4. Use the check boxes in the table to select the members you wish to send the survey to. The top checkbox will select all. 
5. Edit the email. Leave the `magic link` UNEDITED, this is what will link to your survey. You may edit the other text to read as you like.  
  The default message is:  
  "_Hello,_  
  _Please use the following link to automatically login and start the survey:_  
  _%CFS_MAGIC_LINK%._  
  _All the best_"  
6. Select `SEND...`



### Custom documentation
If you need to provide your group with more help resources, you can add custom documentation links that will be available in the sidebar of the group workspace. Examples could include links to websites that provide more information, forums to post questions and discussions, or documentation pages.

To create a custom documentation link:
1. Select **Settings** from the sidebar.
2. Scroll down to **Documentation Links** and select `NEW`.
3. Provide a label (display name) for what the link will be called.
4. Enter the link.
5. Choose if you want all subgroups to also have this link available by ticking the checkbox.
6. Select `SUBMIT`.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/custom_docs.jpg)

