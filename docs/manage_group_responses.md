# Manage Group Responses


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

Group admins can view and edit survey responses from all members of a given group. There are three pathways to find/edit responses:

1. [Edit from Group Responses](#edit-from-group-responses)  
2. [Edit from Results List](#edit-from-results-list)  
3. [Edit from Results Table](#edit-from-results-table)  



### Edit from *Group Responses*
1. Navigate to the correct group workspace (Responses are only visible in the Group Workspace where they were submitted)  
2. Select `Group Responses` from the sidebar menu.  
3. Search for and select the submission that you wish to edit.  
4. Choose the reason for re-submiting the submission.  
5. Select `EDIT ANYWAY`.  
6. This will open the *Survey outline*, navigate to the questions you'd like to edit, after completing your edits, `SUBMIT` the response.  

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/group_responses.jpg)
![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/resubmit_survey.jpg)

### Edit from Results *List*
1. Navigate to the correct survey using the **All Surveys** option in the sidebar.  
2. Select `View Results` from the 3 dot menu (this may also be available in the sidebar if the survey is visible there).  
3. Search or [Filter](https://our-sci.gitlab.io/software/surveystack_tutorials/survey_actions/#filter-results) for the response that you wish to edit from the results `List`.   
4. Open the 3 dot menu for the response and select `Edit`.  
5. Choose the reason for re-submiting the response.  
6. Select `EDIT ANYWAY`  
7. This will open the *Survey outline*, navigate to the questions you'd like to edit, after completing your edits, `SUBMIT` the response.  

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/view_results_to_survey.PNG)
![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/another_view_results_to_survey.PNG)
![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/edit_from_results_tab.PNG)

### Edit from Results *Table*

1. Use the above instructions to navigate to the results page.  
2. Select the `TABLE` option at the top of the page.  
3. Search or [Filter](https://our-sci.gitlab.io/software/surveystack_tutorials/survey_actions/#filter-results) for the response you wish to edit and check the box to its left in the table.   
4. Click `RESUBMIT` in blue in the top left.  
5. Choose the reason for re-submiting the submission.  
6. Select `EDIT ANYWAY`  
7. This will open the *Survey outline*, navigate to the questions you'd like to edit, after completing your edits, `SUBMIT` the response.  

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/results_screen_-_table_view.PNG)