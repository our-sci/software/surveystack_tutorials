# Results View/Download/API

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

You can review the results from any survey by going to that survey's homepage and selecting `RESULTS` from the upper right.

**Note:** **Editing** or **Archiving** a submission or **Viewing private data** will be restricted based on a users **administrative rights**

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/View%20Results/results_page.png "arrow to results tab of a survey")  

`Results` has many features that allow you to view, explore and edit the data:  
- **Basic Filters** to filter the data.  
- **Download** the data.  You can also use these URLs to access the data from other services via **API**.
- **Actions** to edit or archive submissions.  
- Choose to view active data or only **Archived data**.  
- View or **Hide Metadata** for the submissions.  
- Select individual submissions for `ACTIONS`.  
- Use `Page Size` to choose how many submissions to view per page.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/View%20Results/downoad_results.png "Screenshot of the results page with arrows pointing to filters, download button, submission selection, and meta data toggle.")  

### Download results / Access from another service
- Results can be downloaded as a `csv` or as a `json` file. You can also choose to dowload all of the data or just a subset of the data based on filters or just downloading the active page.
- After choosing your download settings, select `DOWNLOAD`.
- **SPECIAL NOTE FOR MATRIX QUESTION TYPES**
- `matrix` question types have a complex results structure that doesn't easily map to a spreadsheet.  They are essentially a table within a single cell.  As a result, when downloading as a CSV you can download or access them in two forms:
   - **Sub rows** - Each `matrix` question type will generate subrows in the csv.  This can make the data more easily visible, but means that the table is not 1 row = 1 submission.
      - to use --> results page link + '&expandAllMatrices=true'
      - **example:** https://app.surveystack.io/api/submissions/csv?survey=60d20b292f38fe0001916497&expandAllMatrices=true
   - **JSON Object** - Each `matrix` question type will have all the results stored in a single cell in a JSON array of objects.  This is a simple structure where each entry in the `matrix` question is an object in the array.  This maintains the 1 row = 1 submission structure, but the matrix data is less human readable.
      - to use --> results page link WITHOUT '&expandAllMatrices=true'
      - **example:** https://app.surveystack.io/api/submissions/csv?survey=60d20b292f38fe0001916497

### Edit submissions
1. Select the submission that you want to edit.
2. Once a submission is checked, an `ACTIONS` bar will appear. Select `RESUBMIT...`
3. Choose the reason for re-submiting the submission.
4. Select `EDIT ANYWAY`
5. This will open the *Survey outline*, navigate to the questions you need to edit, after completing your edits, `SUBMIT` the survey. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/View%20Results/edit_submission.png)

### Archive submissions
Data is never deleted from the SurveyStack platform. However, there may be reasons why you want to remove data from your active dataset. You can do this by **archiving** a submission. In order to be transparent, you will be asked to provide a reason for archiving the submission.  

1. Select the submission that you want to archive.  
2. Once a submission is checked, an `ACTIONS` bar will appear, select `ARCHIVE`  
3. Choose the reason for archiving the submission.  
4. Select `ARCHIVE`

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/View%20Results/archive_submission.png)

Archived submissions can be **restored to the active dataset** by:  
1. Checking `View archived only`.  
2. Selecting the submission that you want to restore.  
3. Selecting `RESTORE`.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/View%20Results/restore_archive.png)

### Filter results
1. Choose the **Field** that you want to filter by (column header).
2. Decide which filters you want to use. Options include
     - equals
     - greater than
     - less than
     - greater than or equal
     - less than or equal
3. Enter the value for filtering that field.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/View%20Results/filter_results.png)
