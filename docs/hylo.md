# Hylo Integration

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

![Example Hylo profile](https://gitlab.com/our-sci/resources/-/raw/master/images/hylo%20example.png)
_Example Hylo profile_

## Description
[Hylo](https://www.hylo.com/) is a community platform for group sharing and communication. Like farmOS (and SurveyStack itself), Hylo has been developed in partnership with OpenTEAM with data privacy and farmer control in mind.

## Surveystack x Hylo
We initially designed the Hylo integration to the same tools and structures as farmOS integration, [beginning with a Common Profile enrollment form](https://our-sci.gitlab.io/software/surveystack_tutorials/enrollment/). With the Common Profile, users can input farm profile details, such as location and crops produced, and send that information directly to farmOS and Hylo at the same time. 

[Common Enrollment Tutorial - Hylo](https://openteamag.gitlab.io/documentation/common-enrollment-guide/#1-integrating-hylo-and-surveystack)

Also check out this [Hylo Onboarding demo survey](https://app.surveystack.io/surveys/643416c728e1190001ba919c). Feel free to start the survey as a user and walk through and enter test data. It is also ok to submit test data, these results are not pushed to any other site or used for analysis.

As of 2023, we updated the [Hylo Onboarding](https://app.surveystack.io/surveys/61d3390f26a0dd00012a842b) and [Contact and Address](https://app.surveystack.io/surveys/6081a2b4a08cfb00014269b5) question sets to create an additional range of options for creating Hylo groups through SurveyStack:  
- Use the full Common Profile + Hylo Onboarding to create a farm profile  
- Use the much more concise Contact and Address questions + Hylo Onboarding question set to create a non-farm group  
- Use all three to create a survey for both farmers and non-farmers, using relevance statements to direct users to the right question set for their needs.

If your SurveyStack group is integrated with Hylo, logged-in users who consent to enrollment using your survey will be added to your network on Hylo as well. Hylo uses a nested group structure that reflects the group structure used in SurveyStack, with members of child groups automatically added to parent groups upon creation. You also have the ability to assign users to groups using API Compose. 

## Additional Resources
Hylo has extensive documentation on their site, and continually builds new resources based on user feedback. Get started in their help documents here: 
[Farm Profiles on Hylo](https://hylozoic.gitbook.io/hylo/guides/farm-profiles-on-hylo)
You can also join the [OpenTEAM Tech Feedback & Support group on Hylo](https://hylozoic.gitbook.io/hylo/guides/farm-profiles-on-hylo) and ask questions directly. 
OpenTEAM has created resources outlining the flow of information between SurveyStack, farmOS, Hylo, and other tools in the tech ecosystem here: [Access Tools and Support](https://openteam.community/access-tools-and-support/)

