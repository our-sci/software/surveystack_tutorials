# Create a survey

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

1. Go to the `All Surveys` page from the sidebar menu of the group workspace
2. Select `Create New Survey`
3. Add a survey name
4. Start adding questions by clicking on the `+` icon. 

    - There are many [question types](https://our-sci.gitlab.io/software/surveystack_tutorials/question_types/#) supported by SurveyStack. 
    - Each question has [properties](https://our-sci.gitlab.io/software/surveystack_tutorials/building_surveys/) to provide hints and more info about the question and to access [advanced features](https://our-sci.gitlab.io/software/surveystack_tutorials/advanced_survey_features/) like [relevance](https://our-sci.gitlab.io/software/surveystack_tutorials/advanced_survey_features/#relevance).
    - Define who can respond to the [survey.](https://our-sci.gitlab.io/software/surveystack_tutorials/building_surveys/#survey-details)
    - Find helpful tips about survey [best practices here](https://our-sci.gitlab.io/software/surveystack_tutorials/best_practices/).

5. Select `SAVE` to save a draft of your survey before you leave. 
6. Once a survey is complete, select `PUBLISH`. ***Note:** Group members cannot respond to surveys until it is published.*

Congratulations! Now that you have a created a survey you can ask your community to [respond](https://our-sci.gitlab.io/software/surveystack_tutorials/respond_to_a_survey/) to it.
