# Navigating the app

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

### Creating an account  

If you want to create a SurveyStack account but do not want to be part of an existing SurveyStack group, you can create an account using the `Register now` button. 

If you are a part of a group, you need to log in to see your group's surveys. To become a member of a group, you should have received an invitation from invitation@surveystack.io (example email below). Click on the `Sign In` button.  

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/2023_surveystack_group_email_invitation.PNG)

LOGGING IN:  

To login to your account without using a password, use the log in button called `EMAIL ME A SIGN IN LINK`. You may continue using the magic sign in email link each time you sign in or, after you're logged into your account, you can create a password.  

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/magic%20link%20login.png)  

</br>

CREATING A PASSWORD:  

To create a password, use the magic sign in link to open your SurveyStack account. From there, navigate to your profile to find the password control panel.  
Note: The magic sign in link will still work even if you've created a password.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/password.png)

</br>

### Add the app to your homescreen

SurveyStack is a progressive web app which means you won't find it in the app or google play store, but you can add it to the homescreen of your smartphone and it will function in the same way as an app. See the instructions below on how to add SurveyStack to your homescreen on different browsers. Adding SurveyStack to your homescreen makes it easier to navigate to the app and it also ensures that you will be able to create and fill-out surveys while offline.

**Installing SurveyStack from Safari**  
1. Go to app.surveystack.io  
2. Select the share options button on the bottom of the screen  
3. Scroll down and select `Add to Home Screen`  
4. Select `Add`  
5. SurveyStack will now be installed on your device and will work offline

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/add%20to%20homescreen/safari%20install.png)  

**Installing SurveyStack from Chrome**  
1. Go to app.surveystack.io  
2. Select the three dots at the top of the browser  
3. Select `Install App`  
4. Select `Install`  
5. SurveyStack will now be installed on your device and will work offline

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/add%20to%20homescreen/chrome%20install.png)  

**Installing Surveystack from Firefox**  
1. Go to app.surveystack.io  
2. There should be a pop up, select `+ Add to homescreen`  
3. Select `Add`  
4. SurveyStack will now be installed on your device and will work offline

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/add%20to%20homescreen/firefox%20install.png)

### Sidebar menu
COLLECT  
- **My Submissions** - once you have started a survey, it will be saved in the `My Submissions` tab. If the survey was both completed and submitted it will be saved in the `SENT` tab, otherwise it will be saved in the `Drafts` tab  
- **Browse** surveys that are in your group, that you have created, or all the surveys available on SurveyStack

ADMIN  
- **Builder** - build your own surveys.  
- **Scripts** - build your own scripts.  
- **Groups** - create your own group.

DOCUMENTATION  
- **SurveyStack help** - (this page) documentation and tutorials on SurveyStack features.  
- **About** - SurveyStack website.

### User profile
View and edit your profile by clicking on the person icon in the upper right corner of the page.

- **Profile**  
- **Edit Account**
- **Select Active Group**
- **Sign Out**

Select the ? icon to return to this page. 

![User profile](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/profile%20and%20help.png)


